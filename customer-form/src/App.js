import React from "react";
import './App.css';
import FormInput from "./components/FormInput";
import { useState } from "react";


const App = () => {
  const [field, setField] = useState({
    fullName:"",
    email:"",
    zipCode:"",
  });

  const [name, setName] = useState("");


  const inputs = [
    {
      id:1,
      onChange: e => setName (e.target.value),
      value: name,
      name:"fullName",
      type:"text",
      placeholder:"Full Name",
      errorMessage: "Must be at least 3 characters",
      label:"Name",
      pattern: "[A-Za-z. ]{3,}",
      required: true,
    },
    {
      id:2,
      name:"email",
      type:"email",
      placeholder:"Email",
      errorMessage: "Must be a valid email address",
      label:"Email Address",
      //pattern: "[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$",
      required:true,
    },
    {
      id:3,
      name:"zipCode",
      type:"number",
      placeholder:"Zip Code",
      errorMessage: "Must be a valid zip Code",
      label:"Zip Code",
      required: true,
    }
]

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  const onChange = (e) => {
    setField({...field, [e.target.name]: e.target.value})
  }

  console.log(field);
  return (
    <div className="App">
     <form onSubmit={handleSubmit}>
      <h1>Form Registration</h1>
      <input value={name} onChange = {e => setName (e.target.value)}/>
      {inputs.map((input) =>(
        <FormInput key={input.id} {...input} field={field[input.name]} onChange={onChange}/>
      ))}
      <button><label>Submit</label></button>
    </form>
  </div>
  )
};

export default App;
