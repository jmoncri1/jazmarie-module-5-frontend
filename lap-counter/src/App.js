import {React, useState} from 'react'
import "./App.css"


export default function App() {
  const [counter, setCounter] = useState(0);
  const [SavedLapCount, setSavedLapCount] = useState([]);
  

  const increment = () => {
    setCounter(count => count + 1);
  };

  const decrement = () => {
    if(counter > 0) {
    setCounter(count => count -1);
    }
  };

  const reset = () => {
    setCounter(0)
  }

  //Save the lap count
  //Create variable to hold lap count
  //Save lap count into variable
  //Display lap count

  return (
    <div className="counter">
      
      <h1>Count Your Laps</h1>
      <div className="buttonHolder">
        <button className="controlButton" onClick={increment}>----------------<h1>+</h1>---------------------</button>
        <button className="controlButton" onClick={decrement}>----------------<h1>-</h1>---------------------</button>
      </div>
      <button className="reset" onClick={reset}>Reset</button>
      <span className="counterDisplay">{counter}</span>
    </div>
  )
}