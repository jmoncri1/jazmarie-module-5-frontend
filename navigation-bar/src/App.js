import React from "react";
import './App.css';
import { Route, Routes} from "react-router-dom";
import { Food } from "./pages/Food";
import { Stuff } from "./pages/Stuff";
import { Login } from "./pages/Login";
import { Milk } from "./pages/Milk";
import { Eggs } from "./pages/Eggs";
import { Bread } from "./pages/Bread";
import { Butter } from "./pages/Butter";
import { Cheese } from "./pages/Cheese";
import { Cookies } from "./pages/Cookies";
import { Juice } from "./pages/Juice";
import { Pancakes } from "./pages/Pancakes";
import { Daipers } from "./pages/Daipers";
import { Water } from "./pages/Water";

function App() {
  return (
    <>
    
     <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/stuff" element={<Stuff/>} />
      <Route path="/login" element={<Login/>} />
      <Route path="/food" element={<Food/>} />
      <Route path="/stuff/milk" element={<Milk/>} />
      <Route path="/stuff/eggs" element={<Eggs/>} />
      <Route path="/stuff/bread" element={<Bread/>} />
      <Route path="/stuff/butter" element={<Butter/>} />
      <Route path="/stuff/cheese" element={<Cheese/>} />
      <Route path="/stuff/cookies" element={<Cookies/>} />
      <Route path="/stuff/juice" element={<Juice/>} />
      <Route path="/stuff/pancakes" element={<Pancakes/>} />
      <Route path="/stuff/daipers" element={<Daipers/>} />
      <Route path="/stuff/water" element={<Water/>} />
      </Routes>
    </>
  )
}

export default App;
