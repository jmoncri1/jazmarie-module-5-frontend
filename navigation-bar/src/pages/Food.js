
import React from "react";
import './App.css';
import { Link } from "react-router-dom";
import shoppingCart from "./fullShoppingCart.jpg";
export function Food() {
    return  (
        <div>
            <nav>
                <div className="logo">
                <h1>Food and Stuff</h1>
                <p><Link to="/"><button>Log Out</button></Link></p>
                <img src={shoppingCart} alt="logo" />
                </div>
                <ul>     
                    <li><Link to="/">Login</Link></li>
                    <li><Link to="/stuff">Stuff</Link></li>
                    <li><Link to="/food">Food</Link></li>
                </ul>
            </nav>
            <h1> Congratulations</h1>
            <h2>You are on the FOOD Page</h2>
        </div>

    )
}