import React from "react";
import './App.css';
import { Link } from "react-router-dom";
import shoppingCart from "./fullShoppingCart.jpg";
export function Login () {
 return (
        <nav>
        <div className="logo">
          <h1>Food and Stuff</h1>
          <img src={shoppingCart} alt="logo" />
        </div>
        <ul>     
          {/* <li><Link to="/">Login</Link></li> */}
          <li><Link to="/stuff">Stuff</Link></li>
          <li><Link to="/food">Food</Link></li>
        </ul>
        <div className="loginInput">
          <label>Username</label>
           <input placeholder="Username" />
           <input placeholder="Password" />
           <Link to="/food"><button id="loginButton">Login</button></Link>
      </div>
      </nav>
            )
    }