import { Link } from "react-router-dom"
export function Bread () {
    return (
        <>
            <h1>Bread</h1> 
            <li><Link to="/stuff">Stuff</Link></li>
        </>
    )
}