import React from "react";
import './App.css';
import { Link } from "react-router-dom";
import shoppingCart from "./fullShoppingCart.jpg";
export function Stuff() {
    return (
        <div>
            <nav>
                <div className="logo">
                <h1>Food and Stuff</h1>
                <img src={shoppingCart} alt="logo" />
                </div>
                <ul>     
                    <li><Link to="/">Login</Link></li>
                    <li><Link to="/stuff">Stuff</Link></li>
                    <li><Link to="/food">Food</Link></li>
                </ul>
            </nav>
            <h1> Congratulations</h1>
            <h2>You are on the STUFF Page</h2>
            <div>
            <ul>
                <li><Link to="/stuff/milk">Milk</Link></li>
                <li><Link to="/stuff/bread">Bread</Link></li>
                <li><Link to="/stuff/eggs">Eggs</Link></li>
                <li><Link to="/stuff/butter">Butter</Link></li>
                <li><Link to="/stuff/cheese">Cheese</Link></li>
                <li><Link to="/stuff/juice">Juice</Link></li>
                <li><Link to="/stuff/water">Water</Link></li>
                <li><Link to="/stuff/diapers">Daipers</Link></li>
                <li><Link to="/stuff/pancakes">Pancakes</Link></li>
                <li><Link to="/stuff/cookies">Cookies</Link></li>
            </ul>
            </div>
        </div>
    )
}