import logo from './logo.svg';
import React from "react";
import './App.css';
import { Route, Routes} from "react-router-dom";
import LoginHomePage from "./components/LoginHomePage"
import Reservations  from './components/Reservations';
import  RoomTypes  from './components/RoomTypes';
import NavigationBar from './components/NavigationBar';
import CreateReservations from './components/CreateReservations';
import useLogin from './components/useLogin';


function App() {

  const {login, isLoggedin, logout} = useLogin()
  return (
    <>
      <h1>Hello</h1>
      <NavigationBar logout={logout} />
      { isLoggedin === false && <LoginHomePage login={login} />}
      { isLoggedin && <Routes>
        <Route path = "/reservations" element={<Reservations />}/>
        <Route path = "reservations/Create" element={<CreateReservations isEdit={false} />} />
        <Route path = "reservations/Edit/:id" element={<CreateReservations isEdit={true} />} />
        <Route path='/room-types' element={<RoomTypes />}/>
      </Routes>}
    </>
  )
}

export default App;
