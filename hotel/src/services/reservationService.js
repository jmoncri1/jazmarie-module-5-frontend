

export async function getReservationById(id) {
  let fetchData = await fetch("http://localhost:8080/reservations/" + id, {
    headers: new Headers({
      "Content-Type": "application/json",
      "mode": "cors",
      'Authorization': `Bearer ${sessionStorage.getItem('token')}`
    })
  });
  let response = await fetchData.json();
  console.log(response);
  return response;
  
}

export async function createReservation(reservation) {
  let fetchData = await fetch("http://localhost:8080/reservations", {
    method: "POST",
    body: JSON.stringify(reservation),
    headers: new Headers({
      "Content-Type": "application/json",
      "mode": 'no-cors',
      'Authorization': `Bearer ${sessionStorage.getItem('token')}`
    })
  });
  let response = await fetchData.json();

  console.log(response);
  return response;
}

export async function updateReservation(reservation) {
  let fetchData = await fetch("http://localhost:8080/reservations", {
    method: "PUT",
    body: JSON.stringify(reservation),
    headers: new Headers({
      "Content-Type": "application/json",
      "mode": 'no-cors',
      'Authorization': `Bearer ${sessionStorage.getItem('token')}`
    })
  });
  let response = await fetchData.json();

  console.log(response);
  return response;
}