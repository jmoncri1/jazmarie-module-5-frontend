import React, {useState} from 'react'
import jwt_decode from 'jwt-decode'
import { useNavigate } from 'react-router-dom';



const useLogin = () => {

    const navigate = useNavigate();
    const [isLoggedin, setIsLoggedin] = useState(false)

    const login = async (email, password) => {

      
        
        const svrResp = await fetch("http://localhost:8080/login", {
          method: "POST",
          headers: new Headers({
            "Content-Type": "application/json",
            "mode": "cors"
          }),
          body: JSON.stringify({email, password})
          
        })
      
        const data = await svrResp.json()
        sessionStorage.setItem("token", data.token);
        const user = jwt_decode(data.token); 
        sessionStorage.setItem("role", user.roles)
        navigate('/reservations')

        setIsLoggedin(true);
      }

      const logout= () => {
        sessionStorage.clear()
        setIsLoggedin(false);

      }

    return {
        login, logout,
        
        
        
        isLoggedin
    };
}

export default useLogin