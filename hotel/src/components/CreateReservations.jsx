import React, { useEffect, useState } from "react";
//import '../App.css';
import { Link, useNavigate, useParams } from "react-router-dom";
import Select from 'react-select'
import Async, { useAsync } from 'react-select/async';
import { createReservation, getReservationById } from "../services/reservationService";
import Reservations from "./Reservations";

export default function CreateReservations (props) {
<h1>Create Reservations</h1>

const [roomTypeOptions, setRoomTypeOptions] = useState("")
const navigate = useNavigate();
const [field, setField] = useState({});
const [email, setEmail] = useState("");
const [checkInDate, setCheckInDate] = useState("");
const [numberOfNights, setNumberOfNights] = useState("");
const [roomType, setRoomType] = useState("");
const [message, setMessage] = useState("");
 
const {id} = useParams();
useEffect(() => {
    if(props.isEdit) {
        const response = getReservationById(id);
        console.log(id);
        console.log(response);
    }
}, [])
let handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const reservation = {
        guestEmail: email,
        checkInDate: checkInDate,
        numberOfNights: numberOfNights,
        roomTypeId: roomType,
      }
      if(props.isEdit) {
        
      }else {
      const response = createReservation(reservation)
      }

    } catch(error) {
        console.warn(error)
    }
}


return (
    <div>
       
            <h1>Create a Reservation</h1>
             <form onSubmit={handleSubmit}>
                <label>Email</label><br/>
                <input type="email" value={email} placeholder= "Email" required onChange={(e) => setEmail(e.target.value)}/><br/>
                <label>Check-in date</label><br/>
                <input type="text" value={checkInDate} placeholder="MM/DD/YYYY" required onChange={(e) => setCheckInDate(e.target.value)}/><br/>
                <label>Number of Nights</label><br/>
                <input type="number" value={numberOfNights} placeholder="Number of Night" required onChange={(e) => setNumberOfNights(e.target.value)}/><br/>
                <label>Room Type</label><br/>
                <select></select>
            <button type="submit">Create</button>
            <div className="message">
                {message ? <p>{message}</p> : null}
            </div>
        </form>
    </div>

);

}