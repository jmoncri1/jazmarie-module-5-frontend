import React, { useEffect, useState } from "react";
//import '../App.css';
import { Link, useNavigate, useParams } from "react-router-dom";
import Select from 'react-select'
import Async, { useAsync } from 'react-select/async';
import { createReservation, getReservationById } from "../services/reservationService";
import Reservations from "./Reservations";

export default function EditReservations (props) {
<h1>Edit Reservations</h1>

const [roomTypeOptions, setRoomTypeOptions] = useState("")
const navigate = useNavigate();
const [field, setField] = useState({});
const [email, setEmail] = useState("");
const [checkInDate, setCheckInDate] = useState("");
const [numberOfNights, setNumberOfNights] = useState("");
const [roomType, setRoomType] = useState("");
const [message, setMessage] = useState("");

const [reservations, setReservations] = useState([])

  useEffect(() => {
    async function fetchData() {
      const getAllReservations = await fetch("http://localhost:8080/reservations", {
        headers: new Headers({
          "Content-Type": "application/json",
          "mode": "cors",
          'Authorization': `Bearer ${sessionStorage.getItem('token')}`
        })
      });
      const allReservations = await getAllReservations.json();
      console.log(allReservations);
      setReservations(allReservations);
    }
    fetchData();
  }, []);

export async function updateReservation(reservation) {
    let fetchData = await fetch("http://localhost:8080/reservations", {
      method: "PUT",
      body: JSON.stringify(reservation),
      headers: new Headers({
        "Content-Type": "application/json",
        "mode": 'no-cors',
        'Authorization': `Bearer ${sessionStorage.getItem('token')}`
      })
    });
    let response = await fetchData.json();
  
    console.log(response);
    return response;
  }