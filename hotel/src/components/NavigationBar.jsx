import React, { useEffect } from 'react';
import "../App.css";
import { Link } from 'react-router-dom';
import { useState } from 'react';
import jwt_decode from 'jwt-decode';

export default function NavigationBar ({logout}){

    
    const role = sessionStorage.getItem("role");
    console.log("role: " + role);
    return (
        <div>
            <nav className='managerNavBar'>
                <ul>
                    {role==="manager" || role==="employee" ? <li><Link to="/reservations">Reservation</Link> </li> : ""}
                    {role === "manager" ? <li><Link to="/room-types">Room Types</Link></li> : ""}
                    <li><button onClick={() => logout()}>Log Out</button></li>
                </ul>
            </nav>
        </div>
        
    )
}