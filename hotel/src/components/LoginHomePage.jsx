import React, { useEffect } from 'react';
import "../App.css";
import { useState } from 'react';


export default function LoginHomePage ({login}) {


  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const formLogin = e => {
    e.preventDefault()
    login(email, password)

  }
  
  return (
    <div className="App">
      <form onSubmit={(e) =>formLogin(e)}>
        <h1>Log In</h1>
        <label>Email</label><br/>
        <input type="email" value={email} required onChange = {e => setEmail (e.target.value)}/><br/>
        <label>Password</label><br/>
        <input type="password" value={password} required onChange = {e => setPassword (e.target.value)}/><br/>
        <button>Login</button>
      </form>
    </div>
  );
}
