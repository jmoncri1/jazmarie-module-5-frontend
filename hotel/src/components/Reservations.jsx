import React, { useEffect, useState } from "react";
//import '../App.css';
import { Link } from "react-router-dom";

export default function Reservations() {


  const [reservations, setReservations] = useState([])

  useEffect(() => {
    async function fetchData() {
      const getAllReservations = await fetch("http://localhost:8080/reservations", {
        headers: new Headers({
          "Content-Type": "application/json",
          "mode": "cors",
          'Authorization': `Bearer ${sessionStorage.getItem('token')}`
        })
      });
      const allReservations = await getAllReservations.json();
      console.log(allReservations);
      setReservations(allReservations);
    }
    fetchData();
  }, []);


  return (
    <div>
      <h1>Reservations</h1>
      <Link to="/reservations/create"><button className=" ">Create</button></Link>
      
      <div className="list-group">
        {
          reservations.length > 0 ? reservations.map((reservation) => {

            return (
              <div className="list-group-item" key={reservation.id}>
                <p className="mb-1 text-center">User: {reservation.user}</p>
                <p className="mb-1">Email: {reservation.guestEmail}</p>
                <Link to={"/reservations/edit/" + reservation.id}><button className=" ">Edit</button></Link>
              </div>
            )
          }) : ""
        }
      </div>


    </div>




  );
}

