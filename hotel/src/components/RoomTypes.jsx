import React, { useEffect, useState } from "react";
//import '../App.css';
import { Link } from "react-router-dom";
import CreateReservations from "./CreateReservations";

export default function RoomTypes() {


  const [roomTypes, setRoomTypes] = useState([])



  useEffect(() => {
    async function fetchData() {
      const getAllRoomTypes = await fetch("http://localhost:8080/room-types", {
        headers: new Headers({
          "Content-Type": "application/json",
          "mode": "cors",
          'Authorization': `Bearer ${sessionStorage.getItem('token')}`
        })
      });
      const allRoomTypes = await getAllRoomTypes.json();
      console.log(allRoomTypes);
      setRoomTypes(allRoomTypes);
    }
    fetchData();
  }, []);


  //   const handleCreateButton = async (e) => {
  //     e.preventDefault();



  return (
    <div>
      <h1>RoomTypes</h1>
      <Link to="/room-types/create"><button className=" ">Create</button></Link>
      <div className="list-group">
        {
          roomTypes.length > 0 ? roomTypes.map((roomType) => {

            return (
              <>
                <div className="list-group-item" key={roomType.id}>
                  <p className="mb-1">Name: {roomType.name}</p>
                  <p className="mb-1">Description: {roomType.description}</p>
                </div>

              </>
            )

          }) : ""

        }
      </div>


    </div>




  );
}