
import './App.css';
import React, {useEffect, useState} from 'react';
import UserCard from './userCard';



function App() {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState("");


    useEffect(() => {

      const fetchApi = async () => {

        try {
          
        
        const response = await fetch("https://641623f30b3cdc7b2ca27737.mockapi.io/ajax/Users")
        const serverData = await response.json();
      
            console.log(serverData)
        if(response.status >= 400){
          setError("Oops something went wrong")
          
        }else {
          setData(serverData)
        }

      } catch (error) { 
          
      }finally {
        setLoading(false)
      }

      }

      fetchApi()


    //     fetch("https://641623f30b3cdc7b2ca27737.mockapi.io/ajax/Users")
    //     .then(response => {
    //         if(response.ok) {
    //             setLoading(false);
    //             return response.json();
    //         } else {
    //             setLoading(false);
    //             setError("Oops something went wrong");
    //         }
    //     }).then(data => {
    //       console.log(data)
    //       setData(data)
    //     } );

    }, []);

  return (
   <div className='app'>
    <div>{error}</div>
    {loading && <div className="spinner"></div>}
    {data.length > 0 && data.map(item =>{
      return(
        <UserCard key={item.id} user={item}/>
      )})}
   </div>
  );
}

export default App;
