import React from "react";

function UserCard (props) {
    const {user} = props;
    return(
        
        <div className="card">
            <div className="card-image">
                <figure className="image is-128x128">
                    <img src={user.avatar} alt="Placeholder image"/>
                </figure>
            </div>
            <div className="card-content">
                <div className="media-content">
                    <p className="title is-4">{user.name}</p>
                </div>
            </div>
        </div>
    )
}

export default UserCard;