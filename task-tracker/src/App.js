import {React, useState} from 'react'
import "./App.css"
import TaskList from './components/taskList';


export default function App() {
  const [taskList, setTaskList] = useState([
      {id: 0, title: "Daily stand-up"},
      {id: 1, title: "Task distribution"},
      {id:2, title: "Collaberate with supervisors"},
      {id:3, title: "Independent working"},
      {id:4, title: "Question and answer session"},
      {id:5, title: "Finalize daily projects"},
      {id:6, title: "Project demonstrations"},
      {id:7, title: "Stand-down"}]);

  const [toDoList, setToDoList] = useState([]);

    function whenTaskIsClicked(task) {
      const newTaskList = taskList.filter(t => t.id !== task.id);
      const newTodoList = toDoList;
      newTodoList.push(task);
      setToDoList(newTodoList); 
      setTaskList(newTaskList); 
    }

    function whenToDoListTaskIsClicked(task) {
      const updatedToDoList = toDoList.filter(t => t.id !== task.id);
      setToDoList(updatedToDoList);
      
      
      
    }
    return  (
      <div className="toDoApp">
        <div className= "taskList">
          <h1>Task List</h1>
          <TaskList tasks={taskList}  whenTaskIsClicked = {whenTaskIsClicked}/>
        </div>
        <div className='toDoList'>
        <h1>To-Do List</h1>
        <TaskList tasks={toDoList} whenTaskIsClicked = {whenToDoListTaskIsClicked}/>
        </div>
      </div>
    )
  }
