import React from "react";
import Task from "./tasks";

export default function TaskList(props) {

   function renderTaskList(){
        if(props.tasks.length > 0) {
          return (
              <ul>
              {props.tasks.map(task => 
            <Task task={task} whenTaskIsClicked={props.whenTaskIsClicked}/>)}   
            </ul>
          )
        } 
    }
    return(
        <div>
            {renderTaskList()}
        </div>

    );
}