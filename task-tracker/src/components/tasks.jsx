import React from "react";

export default function Task(props) {
    const {task} = props;
     return (

        <li onClick={() => props.whenTaskIsClicked(task)}>
            <p>{task.title}</p>
        </li>
       
    );
}